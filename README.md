# Pretex

Pretex is a simple command line utility that is used to parse text files and replacing `#!pretex <command>` lines with the output of that particular command. As of now all commands are directly executed in the shell.

## Installation

Clone the git repository:
    
    $ git clone git@bitbucket.org:Pelto/pretex.git

Build the gem file:
    
    $ gem build pretex.gemspec

Install the gem file:
    
    $ gem install pretex-0.1.0.gem

## Usage

    $ pretex [filepattern]

Pretex will scan the files giver or read from STDIN and output its result to STDOUT.

### Advanced example

Given the following latex file `paper.tex` and a number of markdown files in
the folder `sections`. This example uses [Pandoc](http://http://johnmacfarlane.net/pandoc/) to convert the markdown files to latex.

      \documentclass{article}

      \begin{document}

      \title{Using pandoc with Latex}
      \author{Johannes}
      \maketitle

      #!pretex pandoc -f markdown -t latex sections/*.md

      \end{document}

To generate a pdf with based on `paper.tex` with all the markdown files as their content you simply execute:

      $ pretex paper.tex | pdflatex

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Added some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request