Feature: My bootstrapped app kinda works
  In order to get going on coding my awesome app
  I want to have aruba and cucumber setup
  So I don't have to do it myself

  Scenario: App just runs
    When I get help for "pretex"
    Then the exit status should be 0
    And the banner should be present
    And the banner should document that this app takes options
    And the banner should document that this app's arguments are:
      |filename   |which is optional|
    
  Scenario: Replaces #!pretext run <command> with the output from that command
    Given a file name "test_file.tex" with the line "#!pretex echo hello" in it
    When I successfully run `pretex ../../test_file.tex`
    Then it should output contents but the line "pretex echo hello" should be replaced with "hello"

  Scenario: Save to output file
    Given a file name "test_file.tex" with the line "#!pretex echo hello" in it
    When I successfully run `pretex --out=out_file.tex ../../test_file.tex`
    Then it should output nothing
    And the file "test_file.tex" should have the expected content.    