CONTENTS = [
  '\section{Section 1}',
  'Some text',
  '%{command}',
  'an other line at the end'
]

Given /^a file name "([^"]*)" with the line "([^"]*)" in it$/ do |filename, command|  
  @contents = CONTENTS.map { |line| line % { :command => command } }
  File.open(filename,'w') { |file| @contents.each { |_| file.puts _ } }
end

Then /^it should output contents but the line "(.*?)" should be replaced with "(.*?)"$/ do |given, expected|
  expected_output = CONTENTS.map { |line| line % { :command => expected }} .join("\n")
  assert_exact_output(expected_output, all_output.strip)
end

Then /^I should see the help output$/ do
  step %{the banner should be present}
end

Then /^it should output nothing$/ do
  assert_exact_output('', all_output)
end

Then /^the file "(.*?)" should have the expected content\.$/ do |filename|
  file = File.open(filename, "rb")
  contents = file.read.split("\n")
  assert_success(contents == @contents)
end