# -*- encoding: utf-8 -*-
require File.expand_path('../lib/pretex/version', __FILE__)

Gem::Specification.new do |gem|
  gem.authors       = ["Johannes Pelto-Piri"]
  gem.email         = ["johannes.peltopiri@gmail.com"]
  gem.description   = %q{To help with preprocessing of files, pretex scans the files for #!pretex commands and executes them}
  gem.summary       = %q{Preprocess files}
  gem.homepage      = "http://bitbucket.org/Pelto/pretex"

  gem.files         = `git ls-files`.split($\)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.name          = "pretex"
  gem.require_paths = ["lib"]
  gem.version       = Pretex::VERSION
  gem.add_development_dependency('rdoc')
  gem.add_development_dependency('aruba')
  gem.add_development_dependency('rake','~> 0.9.2')
  gem.add_dependency('methadone', '~>1.0.0.rc4')
end
