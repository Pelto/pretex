module Pretex
  class Preprocessor
    def initialize(options)
      @options = options
    end
    
    def preprocess()
      if @options.has_key?(:output)
        $stdout = File.new(@options[:output], "w")
      end

      ARGF.lines.each do |line|
        puts process_line(line).strip
      end
      $stdout = STDOUT
    end
    
    def process_line(line)
      first, *rest = line.split(/ /)
      
      if first == "#!pretex"
        return execute(rest) rescue ""
      else
        return line
      end
    end
    
    def execute(parts)
      cmd = parts.join(" ")
      Open3.popen3("#{cmd}") { |i, o, e|
        return o.read
      }
    end
  end  
end